﻿using System;
using Bowling.Game;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bowling.Game.Test
{
    [TestClass]
    public class GameTest
    {
        /// <summary>
        /// Calculator reference : http://www.bowlinggenius.com/
        /// </summary>

        [TestClass]
        public class GameTests
        {
            Game game;

            [TestInitialize]
            public void CreateGame()
            {
                game = new Game();
            }

            [TestMethod]
            public void AllOne()
            {
                // Act
                RepeatRoll(pins: 1, count: 20);
                
                // Arrange
                int score = game.GetScore();

                // Assert
                Assert.AreEqual(20, score);
            }

            [TestMethod]
            public void GameWithOneSpare()
            {
                // Act
                Roll(8, 2, 9, 0);
                RepeatRoll(pins: 4, count: 16);
                
                // Arrange
                int score = game.GetScore();

                // Assert
                Assert.AreEqual(92, score);
            }

            [TestMethod]
            public void GameWithOneStrike()
            {
                // Act
                Roll(10, 6, 2);
                RepeatRoll(pins: 3, count: 16);
                
                // Arrange
                int totalScore = game.GetScore();

                // Assert
                Assert.AreEqual(74, totalScore);
            }

            [TestMethod]
            public void PerfectScore()
            {
                // Act
                RepeatRoll(pins: 10, count: 12);

                // Arrange
                int totalScore = game.GetScore();

                // Assert
                Assert.AreEqual(300, totalScore);
            }

            #region private method

            /// <summary>
            /// Method helper 
            /// </summary>
            /// <param name="pins">pins knock down count</param>
            private void Roll(params int[] pins)
            {
                foreach (var pin in pins)
                {
                    game.Roll(pin);
                }
            }

            /// <summary>
            /// Method helper to repeat roll with the same pin knock down
            /// </summary>
            /// <param name="pins">pins knock down count</param>
            /// <param name="count">roll count</param>
            private void RepeatRoll(int pins, int count)
            {
                for (int i = 0; i < count; i++)
                {
                    game.Roll(pins);
                }
            }

            #endregion
        }
        }
}
