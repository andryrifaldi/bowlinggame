﻿using System;

namespace Bowling.Game
{
    public class Game
    {
        // Maximum frame in 1 game
        const int frameTotal = 10;
        // Maximum roll in 1 game
        readonly int[] rolls = new int[21];
        int nextRoll = 0;

        /// <summary>
        /// Set pins knock down each roll
        /// </summary>
        /// <param name="pins">pins knock down</param>
        public void Roll(int pins)
        {
            rolls[nextRoll++] = pins;
        }

        /// <summary>
        /// Get total score 
        /// </summary>
        /// <returns>total score</returns>
        public int GetScore()
        {
            // Reset
            int rollIndex = 0;
            int score = 0;

            // Check if strike
            bool isStrike() {return rolls[rollIndex] == 10;}

            // Check if spare
            bool isSpare() {return rolls[rollIndex] + rolls[rollIndex + 1] == 10;}

            // Calculate strike bonus score
            int strikeBonus() { return rolls[rollIndex + 1] + rolls[rollIndex + 2];}
            // Calculate spare bonus score
            int spareBonus() { return rolls[rollIndex + 2]; }
            // Calculate missed pin score
            int defaultScore() { return rolls[rollIndex] + rolls[rollIndex + 1];}

            for (int frame = 0; frame < frameTotal; frame++)
            {
                if (isStrike())
                {
                    score += 10 + strikeBonus();
                    rollIndex++;
                }
                else if (isSpare())
                {
                    score += 10 + spareBonus();
                    rollIndex += 2;
                }
                else
                {
                    score += defaultScore();
                    rollIndex += 2;
                }
            }

            return score;
        }
    }
}